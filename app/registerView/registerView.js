'use strict';

angular.module('myApp.registerView', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/register', {
            templateUrl: 'registerView/registerView.html',
            controller: 'RegisterCtrl'
        });
    }])

    .controller('RegisterCtrl', ['$rootScope', '$scope', '$http', '$location', '$cookies', 'md5',
        function ($rootScope, $scope, $http, $location, $cookies, md5) {
            self = this;

            self.credentials = {
                'login': '',
                'password': '',
                'confirmPassword': ''
            };
            this.success = true;
            this.triedRegister = false;

            this.validLogin = function () {
                return !self.triedRegister || self.credentials.login.length >= 4;
            };

            this.validPassword = function () {
                return !self.triedRegister || self.credentials.password.length >= 6;
            };

            this.validConfirmPassword = function () {
                return self.credentials.password === self.credentials.confirmPassword;
            };

            this.successfulRegistration = function () {
                return self.success && self.responded;
            };

            this.hashPassword = function (password) {
                var hashed = md5.createHash(password);
                console.log(hashed);
                return hashed;
            };

            this.registerUser = function () {
                console.log('User register invoked.');
                self.triedRegister = true;

                if (!self.validLogin() && !self.validPassword()) {
                    return;
                } else {
                    var registrationDto = {
                        'email': self.credentials.login,
                        'passwordHash': self.hashPassword(self.credentials.password)
                    };

                    $http.post('http://localhost:8080/user/register', registrationDto)
                        .then(
                            function (result) {
                                console.log(result);

                                self.responded = true;
                                self.success = (result.data.status === 'OK');
                            },
                            function (errResult) {
                                console.log(errResult);
                                self.responded = true;
                                self.success = false;
                            });
                }
            };

        }]);