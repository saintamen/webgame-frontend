'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'angular-md5',
    'ngCookies',
    'myApp.viewAddWorld',
    'myApp.viewWorldPreview',
    'myApp.viewCreateColony',
    'myApp.viewEditColonyLot',
    'myApp.loginView',
    'myApp.registerView',
    'myApp.version'
])
    .factory('AuthenticationInterceptor', [function () {
        return {
            // Send the Authorization header with each request
            'request': function (config) {
                config.headers = config.headers || {};
                var encodedString = btoa("user:7be44845-abb8-41e5-abf1-5a40cdd0a4d7");
                config.headers.Authorization = 'Basic ' + encodedString;
                return config;
            }
        };
    }])
    .factory('Authentication', [function () {

        this.isAuthenticated = function () {

        };

        this.isAuthenticated = function () {

        };
    }])
    .config(['$locationProvider', '$routeProvider', '$httpProvider',
        function ($locationProvider, $routeProvider, $httpProvider) {
            $httpProvider.interceptors.push('AuthenticationInterceptor');
            $locationProvider.hashPrefix('!');


            $routeProvider.otherwise({redirectTo: '/viewAddWorld'});
        }])
    .run(function ($rootScope, $location, $cookies, $http) {
        $rootScope.logout = function () {
            console.log("Logging out");

            $cookies.remove('loggedUserId');
            $rootScope.loggedUser = null;
            window.location.reload();
        };

        $rootScope.requestUserColonies = function () {
            $http.get('http://localhost:8080/colony/getUserColonies?userId=' + $rootScope.loggedUser.id)
                .then(
                    function (result) {
                        console.log(result);
                        var colonies = [];
                        for (var index in result.data.body) {
                            colonies.push({
                                'fieldId': result.data.body[index].fieldId,
                                'colonyName': result.data.body[index].colonyName
                            });
                        }
                        $rootScope.loggedUser.colonies = colonies;
                        console.log($rootScope.loggedUser.colonies);
                    },
                    function (errResult) {
                        console.log("Error:");
                        console.log(errResult);
                    });
        };

        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            console.log($rootScope.loggedUser);
            if ($rootScope.loggedUser == null) {
                var loggedUserId = $cookies.get('loggedUserId');
                if (loggedUserId) {
                    $rootScope.loggedUser = {
                        'id': loggedUserId
                    };
                    $rootScope.requestUserColonies();
                } else {
                    if (next.templateUrl === "loginView/loginView.html" ||
                        next.templateUrl === "registerView/registerView.html") {
                    } else {
                        $location.path("/login");
                    }
                }
            }
        });
    });
