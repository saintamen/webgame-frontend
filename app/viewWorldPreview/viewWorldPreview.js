'use strict';

angular.module('myApp.viewWorldPreview', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewWorldPreview', {
            templateUrl: 'viewWorldPreview/viewWorldPreview.html',
            controller: 'WorldPreviewCtrl'
        });
    }])

    .controller('WorldPreviewCtrl', ['$http', function ($http) {
        self = this;

        this.worlds = [];
        this.fields = [];

        this.colony = {
            userId: 1,
            fieldId: 1,
            colonyName: ''
        };

        this.range = function (n) {
            var input = [];
            for (var i = 0; i < n; i += 1) {
                input.push(i);
            }
            return input;
        };

        this.getWorldFields = function () {
            console.log("Retrieving world info.");
            console.log(self.selectedWorld);

            $http.get('http://localhost:8080/world/getWorldFields/' + self.selectedWorld)
                .then(function (response) {
                    console.log(response);

                    self.fields = [];
                    self.selectedWorldSize = Math.sqrt(response.data.body.length);
                    for (var fieldId in response.data.body) {
                        // upchnij elementy do listy
                        self.fields.push(response.data.body[fieldId]);
                    }
                }, function (errResponse) {
                    console.log(errResponse);
                });
        };

        this.retrieveAllWorlds = function () {
            console.log("Retrieving all worlds.");
            $http.get('http://localhost:8080/world/getAllWorlds')
                .then(function (response) {
                    console.log(response);

                    self.worlds = [];
                    for (var worldId in response.data.body) {
                        // upchnij elementy do listy
                        self.worlds.push(response.data.body[worldId]);
                    }
                    self.selectedWorld = self.worlds[0];
                }, function (errResponse) {
                    console.log(errResponse);
                });
        };

        // will retrieve worlds
        this.retrieveAllWorlds();

        this.createColony = function () {
            console.log('clicked creating colony!');
            $http.post('http://localhost:8080/colony/create', self.colony)
                .then(function (result) {
                    console.log(result);
                    if (result.data.status !== 'OK') {
                        self.error = 'Error adding colony!';
                    }
                }, function (result) {
                    console.log(result);
                });
        };

        this.addColony = function (fieldId) {
            console.log('Called creating colony on fieldId:' + fieldId);
            window.location = "/#!/createColony?fieldId=" + fieldId;
        };
    }]);