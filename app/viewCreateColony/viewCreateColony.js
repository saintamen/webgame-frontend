'use strict';

angular.module('myApp.viewCreateColony', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/createColony', {
            templateUrl: 'viewCreateColony/viewCreateColony.html',
            controller: 'CreateColonyCtrl'
        });
    }])

    .controller('CreateColonyCtrl', ['$rootScope', '$scope', '$http', '$location', '$routeParams',
        function ($rootScope, $scope, $http, $location, $routeParams) {
            self = this;

            this.colonyfields = [];

            this.lotPositions = [
                {'x': 30, 'y': 50},
                {'x': 230, 'y': 50},
                {'x': 430, 'y': 50},
                {'x': 30, 'y': 250},
                {'x': 230, 'y': 250},
                {'x': 430, 'y': 250},
                {'x': 30, 'y': 450},
                {'x': 230, 'y': 450},
                {'x': 430, 'y': 450}
            ];

            this.lots = [];

            this.selectedId = $routeParams.fieldId;
            this.createdColony = {
                'colonyName': '', // z formularza
                'fieldId': $routeParams.fieldId,
                'userId': $rootScope.loggedUser.id
            };

            this.fetchFieldInfo = function () {
                console.log('fetching field info');
                $http.get('http://localhost:8080/world/getFieldInfo/' + self.selectedId)
                    .then(function (response) {
                        console.log(response);
                        self.colony = response.data.body.colony;
                        self.field = response.data.body.field;

                        if (self.colony != null) {
                            self.colonyfields = self.colony.lotList;
                            self.lots = [];

                            console.log(self.lots);
                            var buildLots = [];
                            for (var index in self.colonyfields) {
                                buildLots.push({
                                    'x': self.lotPositions[index].x,
                                    'y': self.lotPositions[index].y,
                                    'index': index,
                                    'lot': self.colonyfields[index],
                                    'image': self.getCorrespondingImage(self.colonyfields[index])
                                });
                            }
                            self.lots = buildLots;
                        }
                    }, function (errorData) {
                        console.log(errorData);
                    });
            };

            this.getCorrespondingImage = function (lot) {
                if (lot.buildingType === 'TOWN_HALL') {
                    return '../images/townhall.png';
                } else if (lot.buildingType === 'SAWMILL') {
                    return '../images/sawmill.png';
                } else if (lot.buildingType === 'FARM') {
                    return '../images/farm.png';
                } else if (lot.buildingType === 'ACADEMY') {
                    return '../images/academy.png';
                } else if (lot.buildingType === 'STONEMASON') {
                    return '../images/stonemason.png';
                } else if (lot.buildingType === 'HOUSEHOLD') {
                    return '../images/household.png';
                }

                return '';
            };

            this.isFieldEmpty = function () {
                return self.colony == null;
            };

            this.fetchFieldInfo();

            this.createColony = function () {
                console.log('Create colony clicked');
                console.log(self.createdColony);
                $http.post('http://localhost:8080/colony/create', self.createdColony)
                    .then(function (response) {
                        console.log(response);
                        self.serverResponded = true;
                        if (response.data.status === "OK") {
                            self.success = true;
                            window.location.reload();
                        } else {
                            self.success = false;
                        }
                    }, function (errResponse) {
                        console.log(errResponse);

                        self.serverResponded = true;
                        self.success = false;
                    });
            };

            this.editColonyLot = function (lotIndex) {
                console.log("Clicked editing of: " + lotIndex);
                console.log('owner: ' + self.colony.owner.id);
                console.log('logged in: ' + $rootScope.loggedUser.id);
                if ($rootScope.loggedUser.id == self.colony.owner.id) {
                    window.location = "/#!/viewEditColonyLot?lotId=" + self.colony.lotList[lotIndex].id +
                        "&fieldId=" + self.selectedId;
                }
            };

        }]);