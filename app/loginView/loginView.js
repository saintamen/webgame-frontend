'use strict';

angular.module('myApp.loginView', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/login', {
            templateUrl: 'loginView/loginView.html',
            controller: 'LoginCtrl'
        });
    }])

    .controller('LoginCtrl', ['$rootScope', '$scope', '$http', '$location', 'md5', '$cookies',
        function ($rootScope, $scope, $http, $location, md5, $cookies) {
            self = this;

            this.login = function () {
                console.log('Login invoked.');

                var userLoginDto = {
                    'email': self.credentials.login,
                    'passwordHash': md5.createHash(self.credentials.password)
                };

                $http.post('http://localhost:8080/user/sign-in', userLoginDto)
                    .then(
                        function (result) {
                            console.log(result);
                            if (result.data.status === 'OK') {
                                $rootScope.loggedUser = {
                                    'id': result.data.body.id
                                };
                                $cookies.put('loggedUserId', result.data.body.id);
                                $rootScope.requestUserColonies();
                            }
                        },
                        function (errResult) {
                            console.log("Error:");
                            console.log(errResult);
                        });
            };

        }]);