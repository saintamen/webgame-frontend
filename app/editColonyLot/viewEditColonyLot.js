'use strict';

angular.module('myApp.viewEditColonyLot', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewEditColonyLot', {
            templateUrl: 'editColonyLot/viewEditColonyLot.html',
            controller: 'EditColonyLotCtrl'
        });
    }])

    .controller('EditColonyLotCtrl', ['$http', '$routeParams', function ($http, $routeParams) {
        self = this;
        this.buildingTypes = [];
        this.currentLotId = $routeParams.lotId;
        this.currentFieldId = $routeParams.fieldId;
        self.looper = null;

        this.createBuildingDto = {
            'userId': 1,
            'lotId': $routeParams.lotId,
            'buildingType': ''
        };

        this.fetchBuildingTypes = function () {
            $http.get('http://localhost:8080/colony/getBuildingTypes')
                .then(function (result) {
                    console.log(result);
                    self.buildingTypes = [];
                    for (var buildingTypeIndex in result.data.body) {
                        // upchnij elementy do listy
                        self.buildingTypes.push(result.data.body[buildingTypeIndex]);
                    }
                }, function (errResponse) {
                    console.log(errResponse);
                });
        };

        this.isEmptyLot = function () {
            if (self.lot != null) {
                return self.lot.buildingType === 'NONE';
            }
            return false;
        };

        this.build = function () {
            $http.post('http://localhost:8080/colony/createBuilding', self.createBuildingDto)
                .then(function (result) {
                    console.log(result);
                    window.location.reload();
                }, function (result) {
                    console.log(result);
                });
        };

        this.howManySecondsUntil = function () {
            if (self.lot != null && self.lot.upgradeFinishTime != null) {
                var strDate = self.lot.upgradeFinishTime;
                var dateParts = strDate.split("-");

                var date = new Date(dateParts[0], (dateParts[1] - 1), dateParts[2], dateParts[3], dateParts[4], dateParts[5]);
                var now = new Date();
                var dif = (date.getTime() - now.getTime()) / 1000;
                console.log(dif);

                return dif;
            }
            return 0;
        };

        this.redirectToColony = function () {
            window.location = "/#!/createColony?fieldId=" + self.currentFieldId;
        };

        this.updateTimeToGo = function () {
            try {
                self.timeToGo = self.howManySecondsUntil();
            }
            catch (err) {
                clearInterval(self.looper);
                return 0;
            }

            if (self.timeToGo <= 0) {
                self.timeToGo = 0;
                if (self.looper != null) {
                    clearInterval(self.looper);
                    window.location.reload();
                    self.looper = null;
                }
            }

            var strTime = self.timeToGo + '';
            document.getElementById("timeLeft").innerText = strTime.split(".")[0];
        };

        this.updateTime = function () {
            if (self.timeToGo > 0 && self.looper == null) {
                self.looper = setInterval(self.updateTimeToGo, 500);
            }
        };

        this.fetchLotInfo = function () {
            $http.get('http://localhost:8080/colony/getLotInfo/' + self.currentLotId)
                .then(function (response) {
                    console.log(response);
                    self.lot = response.data.body;
                    self.updateTimeToGo();
                    self.updateTime();
                }, function (response) {
                    console.log(response);
                });
        };
        this.fetchLotInfo();
        this.fetchBuildingTypes();
    }]);