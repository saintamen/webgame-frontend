'use strict';

angular.module('myApp.viewAddWorld', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewAddWorld', {
            templateUrl: 'viewAddWorld/viewAddWorld.html',
            controller: 'AddWorldCtrl'
        });
    }])

    .controller('AddWorldCtrl', ['$http', '$scope', function ($http, $scope) {
        self = this;
        $scope.selectedTab = 'addWorld';
        var serverResponded = false;

        this.world = {
            name: '',
            sizeRowsColumns: 0
        };

        this.createWorld = function () {
            console.log('Create world request has been sent.');
            $http.post("http://localhost:8080/world/create", self.world)
                .then(
                    function (response) {
                        console.log(response);

                        // setting proper variables to show communicate on site
                        self.serverResponded = true;
                        if (response.data.status === "OK") {
                            self.success = true;

                            document.getElementById("sizeRowsColumns").value = "";
                            document.getElementById("name").value = "";
                        } else {
                            self.success = false;
                        }
                    }, function (result) {
                        console.log(result);

                        self.serverResponded = true;
                        self.success = false;
                    });
        };
    }]);