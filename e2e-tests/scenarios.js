'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {


  it('should automatically redirect to /viewAddWorld when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/viewAddWorld");
  });


  describe('viewAddWorld', function() {

    beforeEach(function() {
      browser.get('index.html#!/viewAddWorld');
    });


    it('should render viewAddWorld when user navigates to /viewAddWorld', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for view 1/);
    });

  });


  describe('viewWorldPreview', function() {

    beforeEach(function() {
      browser.get('index.html#!/viewWorldPreview');
    });


    it('should render viewWorldPreview when user navigates to /viewWorldPreview', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for view 2/);
    });

  });
});
